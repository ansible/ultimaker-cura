# Ultimaker Cura

This Ansible role installs AppImage of either a specified version or
the latest available version.


## XDG desktop entry

This role uses the `xdg_desktop_entries` and `xdg_desktop_icons` variables,
which are defined in the playbook `group_vars`:

```
xdg_desktop_entries: "{{ xdg_data_dir }}/applications"
xdg_desktop_icons: "{{ xdg_data_dir }}/icons"
```

with `xdg_data_dir` in turn defined inside this role.
